class Group(object):
    def __init__(self, elements):
        '''
        elementsは置換群として以下の形式のdictとして保持
        {
            'e': {1:1, 1:2, 1:3},  # 単位元
            'c1': {1:2, 2:3, 3:1},  # 回転(60°)
            'c2': {1:3, 2:1, 3:2},  # 回転(120°)
            's1': {1:1, 2:3, 3:2},  # 鏡映(1を軸)
            's2': {1:3, 2:2, 3:1},  # 鏡映(2を軸)
            's3': {1:2, 2:1, 3:3},  # 鏡映(3を軸)
        }
        '''
        self.elements = elements
        #self.elements = {k: {i[0]:i[1] for i in sorted(v.items(), key=lambda x:x[0])} for k, v in elements.items()}
        self.width = max(map(len, elements.keys()))

    def symbol(self, d):
        '''
        置換群の変換テーブルから記号の文字列を返す(string)
        '''
        ret = None
        for k, v in self.elements.items():
            if d == v:
                ret = k
                break
        # エラー処理
        if ret is None:
            print('\n=====error!!======')
            print(d, 'is not exists.')
        return ret

    def prod(self, g1, g2):
        '''
        引数、戻り値はstring
        g1 * g2 の積
        '''
        prod_el = {}
        el1 = self.elements[g1]
        el2 = self.elements[g2]
        for key in el2.keys():
            prod_el[key] = el1[el2[key]]
        return self.symbol(prod_el)

    def show_prod_table(self):
        target = list(self.elements.keys())
        # header
        for t in [' '] + target:
            print(t.ljust(self.width, ' '), end='|')

        # prod
        for g1 in target:
            print('\n{}'.format(g1.ljust(self.width, ' ')), end='|')
            for g2 in target:
                g = self.prod(g1, g2)
                print(g.ljust(self.width, ' '), end='|')
        print()
