import unittest
from group_theory import Group


class TestGroupTheory(unittest.TestCase):
    def test_s3v(self):
        '''
        正三角形の合同変換群
        '''
        elements = {
            'e': {1:1, 2:2, 3:3},  # 単位元
            'c1': {1:2, 2:3, 3:1},  # 回転(60°)
            'c2': {1:3, 2:1, 3:2},  # 回転(120°)
            's1': {1:1, 2:3, 3:2},  # 鏡映(1を軸)
            's2': {1:3, 2:2, 3:1},  # 鏡映(2を軸)
            's3': {1:2, 2:1, 3:3},  # 鏡映(3を軸)
        }
        g = Group(elements)
        g.show_prod_table()

    def test_regular_tetrahedron_rotation_group(self):
        '''
        正四面体回転群
        '''
        elements = {
            'e': {1:1, 2:2, 3:3, 4:4},
            'a': {1:2, 2:1, 3:4, 4:3},
            'b': {1:3, 2:4, 3:1, 4:2},
            'c': {1:4, 2:3, 3:2, 4:1},
            'i1': {1:1, 2:4, 3:2, 4:3},
            'i2': {1:3, 2:2, 3:4, 4:1},
            'i3': {1:4, 2:1, 3:3, 4:2},
            'i4': {1:2, 2:3, 3:1, 4:4},
            'ii1': {1:1, 2:3, 3:4, 4:2},
            'ii2': {1:4, 2:2, 3:1, 4:3},
            'ii3': {1:2, 2:4, 3:3, 4:1},
            'ii4': {1:3, 2:1, 3:2, 4:4},
        }
        g = Group(elements)

        checks = [
            ['a', 'e', 'a'],
            ['b', 'e', 'b'],
            ['c', 'e', 'c'],
            ['i1', 'e', 'i1'],
            ['i2', 'e', 'i2'],
            ['i3', 'e', 'i3'],
            ['i4', 'e', 'i4'],
            ['ii1', 'e', 'ii1'],
            ['ii2', 'e', 'ii2'],
            ['ii3', 'e', 'ii3'],
            ['ii4', 'e', 'ii4'],
            ['b', 'a', 'c'],
            ['a', 'i1', 'i4'],
            ['b', 'i1', 'i2'],
            ['c', 'i1', 'i3'],
            ['i1', 'ii2', 'b'],
            ['ii2', 'i1', 'c'],
        ]
        for c in checks:
            self.assertEqual(g.prod(c[0], c[1]), c[2])
        g.show_prod_table()


if __name__ == '__main__':
    unittest.main()
